import grid from 'ui:grid.avc';
import icon from './assets/icon.png';
import {ITab, IGrid, IEditor } from "C:/Users/user/Desktop/avrorajs/@os/ui/components/exporter.js";
//import {ISystemFiles} from 'C:/Users/user/Desktop/avrorajs/@os/applications/system/exporter.js';

@computed()
export class Projects extends IApplication{
    constructor() {
        super();
        this.db      = new INEDB(__dirname,'./GoogleProjects.db');
        this.compute = require('@google-cloud/compute');
    }
    async addProject(key){
        try{
            JSON.parse(key.trim())
        }catch (e) {
            return {error : 'not valid key'}
        }
        let project = {
            credentials : JSON.parse(key.trim()), date : new Date()
        }
       
        let compute = new IGoogle.compute({
            projectId : project.credentials.project_id, credentials: project.credentials
        });
    
        try{
            await IGoogle.list.call({
                compute : compute
            });
    
            return await new Promise((x) => {
                this.db.insert(project, function (err, projects) {
                    x(projects)
                });
            })
            
        }catch (e) {
            return {error : 'not valid key'}
        }
        
        return compute;
    }
    list(){
        return new Promise((x) => {
            this.db.find({}, function (err, docs) {
                x(docs);
            });
        })
    }
    delete(id){
    
    }
}

@computed()
export class IGoogle extends IApplication{
    constructor() {
        super();
    }
    compute = require('@google-cloud/compute')
    async create(name){
        const zone = this.compute.zone('us-central1-c');
        const [vm, operation] = await zone.createVM(name, {os: 'ubuntu'});
        await operation.promise();
        return vm
    }
    async list(){
        let vms = await this.compute.getVMs({
            maxResults: 10,
        });
        return vms.filter(x => x && x.id);
    }
    async instances(){
        let list = (await this.list()).filter(x => x && x.items && x.id);
        let vms  = {};
        for(let i in list){
            if(list[i].items)
                for(let zone in list[i].items){
                    if(list[i].items[zone].instances){
                        if(!vms[zone])vms[zone] = list[i].items[zone].instances
                    }
                }
        }
        return vms
    }
    delete(){
    
    }
}

@computed()
export class DB extends IApplication{
    constructor() {
        super();
        return new INEDB(__dirname,'./GoogleProjects.db');
    }
}

export class IGoogleComputes {
    @api.compute init(){
        this.projects = [];
        
        DB.find({},  (err, projects) => {
            projects.forEach(project => {
                this.projects.push(
                  new (require('@google-cloud/compute'))({
                      projectId   : project.credentials.project_id,
                      credentials : project.credentials
                  })
                )
            });
        });
    }
    /** add a new project key **/
    @api async addProjectKey(key){
        
        try{
            JSON.parse(key.trim())
        }catch (e) {
            return {error : 'not valid key'}
        }
        
        let config = {
            credentials : JSON.parse(key.trim()), date : new Date()
        }
    
        let compute = new IGoogle.compute({
            projectId : config.credentials.project_id, credentials: config.credentials
        });
    
        try{
            let project = await IGoogle.list.call({
                compute : compute
            });
        
            return await new Promise((x) => {
                DB.insert(config,  (err, projects) => {
                    project.credentials = config.credentials
                    this.projects.push(project);
                    x(true);
                });
            });
        }catch (e) {
            return {error : e}
        }
        
    }
    /** return list of projects **/
    @api async getProjects(){
        console.log('get proj')
        let projects = [];
        for(let i in this.projects){
            let list = (await this.projects[i].getVMs({
                maxResults: 10,
            })).filter(x => x && x.items && x.id);
            
            let vms  = {};
            for(let i in list){
                if(list[i].items)
                    for(let zone in list[i].items){
                        if(list[i].items[zone].instances){
                            if(!vms[zone])vms[zone] = list[i].items[zone].instances
                        }
                    }
            }
            
            let children = [];
            for(let zone in vms){
                let names = []
                vms[zone].forEach( z => {
                    names.push({
                        recid: _.randomID(),
                        name : z.name,
                        vm   : vms[zone]
                    })
                });
                children.push({
                    recid: _.randomID(),
                    name : zone,
                    w2ui : {
                        children : names
                    }
                });
            }
    
            children.unshift({
                recid : _.randomID(),
                name  : '<span class="add-new-vm" style="margin-left: -15px;"><i class="fas fa-plus"></i> Add virtual machine</span>'
            });
            
            projects.push({
                recid : _.randomID(),
                name  : this.projects[i].projectId,
                w2ui  : {
                    children : children
                }
            })
        }
        
        /** if no project added, return a tree with the option to add a new project **/
        if(!projects.length){
            projects.push({
                recid : _.randomID(),
                name  : '<i class="fas fa-laptop-code"></i> <b>No Projects found</b>',
                w2ui  : {
                    children : [{
                        recid: _.randomID(),
                        name : '<span class="add-new-project"><i class="fas fa-plus"></i> Add new project</span>'
                    }]
                }
            })
        }else{
            /** if projects array is not empty, prepend a record with option to add another project **/
            projects.unshift({
                recid : _.randomID(),
                name  : '<span class="add-new-project"><i class="fas fa-plus"></i> Add new project</span>'
            })
        }
        return projects;
    }
    
    @api list(){
        return IGoogle.list()
    }
    
}

class KeySelector{
    constructor(config){
        return new AVC(config.ID, require('./keyselector.avc').default, () => {
            return config
        })
    }
}
class VMCreator{
    constructor(config){
        return new AVC(config.ID, require('./vmcreator.avc').default, () => {
            return config
        })
    }
}

export const GoogleCompute = {
    ID       : 'GoogleCompute',
    window   : function GoogleCompute(ID,Window) {
        let GID  = 'GoogleCompute' + ID;
        
        let size = {
            width  : 550,
            height : 500
        }, workspace;
        
        let win = {
            ID       : GID,
            title    : 'Google Compute',
            /** window size **/
            size     : size,
            /** window position option, z stands for zIndex **/
            layout   : {
                ID      : GID + 'layout',
                padding : 5,
                panels  : [{
                    type      : 'left',
                    resizable : true,
                    size      : 250,
                    component : new ITab({
                        ID    : GID  + 'LeftTab',
                        tabs  : [
                            {   id   : 'tab1',
                                text : 'Projects',
                                component : new IGrid({
                                    ID : GID +  'InstancesGrid',
                                    columns : {
                                        list : [{
                                            field: 'name',
                                            text : 'name'
                                        }]
                                    },
                                    show : {
                                        columnHeaders : false
                                    },
                                    records : [],
                                    events: {
                                        onAnyEvent(){
                                            let pbtn = this.box.querySelector('.add-new-project');
                                            if(pbtn !== null && !pbtn.init){
                                                let cb = () => {
                                                    workspace.windows.push({
                                                        key     : _.randomID(),
                                                        window  : function(ID,Window,Properties,Events){
                                                            let selectButton, key;
                                                            return {
                                                                ID          : 'KeySelector' + ID,
                                                                size        : Properties.size,
                                                                layout      : {
                                                                    ID : 'KeySelectorLayout' + ID,
                                                                    padding : 5,
                                                                    panels  : [{
                                                                        type: 'main',
                                                                        resizable: true,
                                                                        size: 200,
                                                                        component: new KeySelector({})
                                                                    }]
                                                                },
                                                                parentWindow : Properties.parentWindow,
                                                                buttons    : [{
                                                                    label    : 'Select',
                                                                    disabled : true,
                                                                    events   : {
                                                                        async onClick(){
                                                                            selectButton.loading = true;
                                                                            selectButton.update();
                            
                                                                            IGoogleCompute.addProjectKey(key).then((result) => {
                                                                                console.log(result)
                                                                                //Window.close();
                                                                            })
                                                                        },
                                                                        onMounted(){
                                                                            selectButton = this;
                                                                        }
                                                                    }
                                                                }],
                                                                events : {
                                                                    onMounted(){
                                                                        let cb = (event) => {
                                                                            if(event.target.value.length){
                                                                                selectButton.disabled = false;
                                                                            }else{
                                                                                selectButton.disabled = true;
                                                                            }
                                                                            key = event.target.value;
                                                                            selectButton.update()
                                                                        };
                                                                        this.$('.key-selector').addEventListener('keyup',cb);
                                                                        this.$('.key-selector').addEventListener('paste',cb);
                                                                    }
                                                                }
                                                            }
                                                        },
                                                        properties : {
                                                            size   : {
                                                                width  : 450,
                                                                height : 400
                                                            },
                                                            parentWindow : {
                                                                ID : GID
                                                            }
                                                        }
                                                    });
                                                    workspace.update();
                                                }
                                                pbtn.init = true;
                                                pbtn.removeEventListener('click',cb);
                                                pbtn.addEventListener('click',cb,false);
                                            }
    
                                            let vbtn = this.box.querySelector('.add-new-vm');
                                            if(vbtn !== null && !vbtn.init){
                                                let cb = () => {
                                                    workspace.windows.push({
                                                        key     : _.randomID(),
                                                        window  : function(ID,Window,Properties,Events){
                                                            let selectButton, key;
                                                            return {
                                                                ID          : 'VMSelector' + ID,
                                                                size        : Properties.size,
                                                                layout      : {
                                                                    ID : 'VMSelector' + ID,
                                                                    padding : 5,
                                                                    panels  : [{
                                                                        type: 'main',
                                                                        resizable: true,
                                                                        size: 200,
                                                                        component: new VMCreator({})
                                                                    }]
                                                                },
                                                                parentWindow : Properties.parentWindow,
                                                                buttons    : [{
                                                                    label    : 'Select',
                                                                    disabled : true,
                                                                    events   : {
                                                                        async onClick(){
                                                                            selectButton.loading = true;
                                                                            selectButton.update();
                                    
                                                                            IGoogleCompute.createProject(key).then((result) => {
                                                                                console.log(result)
                                                                                //Window.close();
                                                                            })
                                                                        },
                                                                        onMounted(){
                                                                            selectButton = this;
                                                                        }
                                                                    }
                                                                }],
                                                                events : {
                                                                    onMounted(){
                                                                        let cb = (event) => {
                                                                            if(event.target.value.length){
                                                                                selectButton.disabled = false;
                                                                            }else{
                                                                                selectButton.disabled = true;
                                                                            }
                                                                            key = event.target.value;
                                                                            selectButton.update()
                                                                        };
                                                                        this.$('.key-selector').addEventListener('keyup',cb);
                                                                        this.$('.key-selector').addEventListener('paste',cb);
                                                                    }
                                                                }
                                                            }
                                                        },
                                                        properties : {
                                                            size   : {
                                                                width  : 450,
                                                                height : 400
                                                            },
                                                            parentWindow : {
                                                                ID : GID
                                                            }
                                                        }
                                                    });
                                                    workspace.update();
                                                }
                                                vbtn.init = true;
                                                vbtn.removeEventListener('click',cb);
                                                vbtn.addEventListener('click',cb,false);
                                            }
                                        },
                                        onMounted(){
                                            IGoogleCompute.getProjects().then(projects => {
                                                this.records = projects;
                                                this.records.forEach((r,i) => {
                                                    if(r.w2ui){
                                                        this.expand(r.recid);
                                                    }
                                                })
                                                this.refresh();
                                            })
                                        }
                                    }
                                })
                            }
                        ]
                    })
                },{
                    type      : 'main',
                    resizable : true,
                    component : new ITab({
                        ID    : GID  + 'MainTab',
                        tabs  : [
                            {   id   : 'tab1',
                                text : 'Instances Settings',
                                component : new IGrid({
                                    ID : GID +  'SettingGrid',
                                    columns : {
                                        list : [{
                                            field: 'name',
                                            text : 'name'
                                        }]
                                    },
                                    show : {
                                        columnHeaders : false
                                    },
                                    records : [],
                                    events: {}
                                })
                            }
                        ]
                    })
                }],
                events : {
                    onMounted(){
                    
                    },
                    onResize: function(event) {
                    }
                }
            },
            events : {
                onMounted(){
                    workspace = this.getWorkspace();
                }
            }
        }
        return win;
    },
    icon     : {
        image : icon,
        title : 'Google Compute',
        ID    : 'googleCompute'
    },
    settings : {
        launchOnStartup : true
    }
}